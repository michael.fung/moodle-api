import {
  objectToParams,
  subObjectToParams,
  arrayToParams
} from '../../src/utils/ParamBuilder';

describe('objectToParams', () => {
  test('{ test: 1 }', () => {
    expect(objectToParams({ test: 1 })).toEqual({ test: 1 });
  });

  test('{ test1: 1, test2: "2" }', () => {
    expect(objectToParams({ test1: 1, test2: '2' })).toEqual({
      test1: 1,
      test2: '2'
    });
  });

  test('{ test1: 1, test2: "2", test3: true }', () => {
    expect(objectToParams({ test1: 1, test2: '2', test3: true })).toEqual({
      test1: 1,
      test2: '2',
      test3: 1
    });
  });

  test('{ test1: 1, test2: "2", test3: true, test4: [1] }', () => {
    expect(
      objectToParams({ test1: 1, test2: '2', test3: true, test4: [1] })
    ).toEqual({ test1: 1, test2: '2', test3: 1, 'test4[0]': 1 });
  });

  test('{ test1: 1, test2: "2", test3: true, test4: [ { test5: 3 }, 4] }', () => {
    expect(
      objectToParams({
        test1: 1,
        test2: '2',
        test3: true,
        test4: [{ test5: 3 }, 4]
      })
    ).toEqual({
      test1: 1,
      test2: '2',
      test3: 1,
      'test4[0][test5]': 3,
      'test4[1]': 4
    });
  });
});

describe('subObjectToParams', () => {
  test('{ test1: { test2: 1 } }', () => {
    expect(subObjectToParams('test1', { test2: 1 })).toEqual({
      'test1[test2]': 1
    });
  });

  test('{ test1: { test2: { test3: 1 }}}', () => {
    expect(subObjectToParams('test1', { test2: { test3: 1 } })).toEqual({
      'test1[test2][test3]': 1
    });
  });

  test('{ test1: { test2: { test3: { test4: 1 } }}}', () => {
    expect(
      subObjectToParams('test1', { test2: { test3: { test4: 1 } } })
    ).toEqual({ 'test1[test2][test3][test4]': 1 });
  });

  test('no object', () => {
    expect(subObjectToParams('test1', {})).toEqual({});
  });
});

describe('arrayToParams', () => {
  test('{ test1: [1] }', () => {
    expect(arrayToParams('test1', [1])).toEqual({ 'test1[0]': 1 });
  });

  test('{ test1: [1, 2] }', () => {
    expect(arrayToParams('test1', [1, 2])).toEqual({
      'test1[0]': 1,
      'test1[1]': 2
    });
  });

  test('{ test1: [1, 2, true] }', () => {
    expect(arrayToParams('test1', [1, 2, true])).toEqual({
      'test1[0]': 1,
      'test1[1]': 2,
      'test1[2]': 1
    });
  });

  test('{ test1: [ { test2: 1 } ] }', () => {
    expect(arrayToParams('test1', [{ test2: 1 }])).toEqual({
      'test1[0][test2]': 1
    });
  });

  test('{ test1: [ { test2: 1 }, { test3: 2} ] }', () => {
    expect(arrayToParams('test1', [{ test2: 1 }, { test3: 2 }])).toEqual({
      'test1[0][test2]': 1,
      'test1[1][test3]': 2
    });
  });

  test('{ test1: [ { test2: { test3: 1 } }, { test4: 2 } ] }', () => {
    expect(
      arrayToParams('test1', [{ test2: { test3: 1 } }, { test4: 2 }])
    ).toEqual({ 'test1[0][test2][test3]': 1, 'test1[1][test4]': 2 });
  });

  test('{ test1: [ { test2: [ { test3: 1, test4: 2 } ] } ] }', () => {
    expect(
      arrayToParams('test1', [ { test2: [ { test3: 1, test4: 2 } ] } ])
    ).toEqual({ 'test1[0][test2][0][test3]': 1, 'test1[0][test2][0][test4]': 2 });
  });
});
