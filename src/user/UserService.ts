import HttpClient from '../utils/HttpClient';
import { objectToParams } from '../utils/ParamBuilder';
import { ICriteria } from '../common/Common';
import { IUser } from './User';

export default class UserService {
  private static instance: UserService
  
  public static getInstance = (httpClient: HttpClient) => {
    if(!UserService.instance) {
      UserService.instance = new UserService(httpClient);
    }
    return UserService.instance;
  }

  constructor(private httpClient: HttpClient) {}

  /**
   * Search Users
   * 
   * @param {ICriteria} criteria
   */
  public search = async (criteria: ICriteria[]) => {
    const params = objectToParams({
      wsfunction: 'core_user_get_users',
      criteria
    });

    return await this.httpClient.get(params);
  };

  /**
   * Update Users
   * 
   * @param {IUser[]} users
   */
  public update = async (users: IUser[]) => {
    const params = objectToParams({
      wsfunction: 'core_user_update_users',
      users
    });

    return await this.httpClient.get(params);
  };
}
