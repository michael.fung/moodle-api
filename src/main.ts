import HttpClient from "./utils/HttpClient";
import CategoryService from "./category/CategoryService";
import CohortService from "./cohort/CohortService";
import UserService from "./user/UserService";

class Main {
    private static instance: Main
  
    public static getInstance = () => {
        if(!Main.instance) {
            Main.instance = new Main();
        }
        return Main.instance;
    }

    public init = (baseUrl: string, token: string) => {
        const httpClient = HttpClient.getInstance(baseUrl, token)
        return {
            categories: CategoryService.getInstance(httpClient),
            cohorts:  CohortService.getInstance(httpClient),
            users: UserService.getInstance(httpClient)
        }
    }
}

export const MoodleApi = (baseUrl: string, token: string) => {
    return Main.getInstance().init(baseUrl, token)
}
