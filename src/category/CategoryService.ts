import HttpClient from '../utils/HttpClient';
import { objectToParams } from '../utils/ParamBuilder';

import { ICriteria } from '../common/Common';
import { ICategory } from './Category';

export default class CategoryService {
  private static instance: CategoryService
  
  public static getInstance = (httpClient: HttpClient) => {
    if(!CategoryService.instance) {
      CategoryService.instance = new CategoryService(httpClient);
    }
    return CategoryService.instance;
  }
  
  constructor(private httpClient: HttpClient) {}

  /**
   * @param {Criteria[]} criteria
   */
  public search = async (criteria: ICriteria[]) => {
    const params = objectToParams({
      wsfunction: 'core_course_get_categories',
      criteria
    });

    return await this.httpClient.get(params);
  }

  /**
   * @param {Category[]} category
   */
  public create = async (categories: ICategory[]) => {
    const params = objectToParams({
      wsfunction: 'core_course_create_categories',
      categories
    });

    return await this.httpClient.get(params);
  }
}
