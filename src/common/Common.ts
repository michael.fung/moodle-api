export interface IContext {
  id: number;
}

export interface ICriteria {
  key: string;
  value: string;
}

export interface ITypeValue {
  type: string
  value: string
}