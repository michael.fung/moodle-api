import axios, { AxiosResponse } from 'axios';

export default class HttpClient {
  private static instance: HttpClient
  
  public static getInstance = (baseUrl: string, token: string) => {
    if(!HttpClient.instance) {
      HttpClient.instance = new HttpClient(baseUrl, token);
    }
    return HttpClient.instance;
  }
  
  private config: object

  constructor(private baseUrl: string, token: string) {
    this.config = {
      wstoken: token,
      moodlewsrestformat: 'json'
    }
  }

  public get = async (params: object) => {
    const res = await axios.get(`${this.baseUrl}`, { params: { ...params, ...this.config }});
    return this.returnData(res);
  };

  public post = async (params: object) => {
    const res = await axios.post(`${this.baseUrl}`, { params: { ...params, ...this.config }});
    return this.returnData(res);
  };

  private returnData = (res: AxiosResponse) => {
    if (res.status === 200) {
      return res.data;
    }

    const { status, statusText } = res;
    return { status, statusText };
  };
}
