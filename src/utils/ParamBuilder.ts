export const objectToParams = (obj: object): object => {
  return Object.keys(obj).reduce((params, key: string) => {
    const element = obj[key];
    if (Array.isArray(element)) {
      return { ...params, ...arrayToParams(key, element) };
    } else if (typeof element === 'object') {
      return { ...params, ...subObjectToParams(key, element) };
    }

    params[key] = typeof element === 'boolean' ? +element : element;
    return params;
  }, {});
};

export const subObjectToParams = ( parentKey: string, obj: object): object => {
  return Object.keys(obj).reduce((params, key: string) => {
    const element = obj[key];

    if (typeof element === 'object') {
      return {
        ...params,
        ...subObjectToParams(`${parentKey}[${key}]`, element)
      };
    }

    params[`${parentKey}[${key}]`] =
      typeof element === 'boolean' ? +element : element;
    return params;
  }, {});
};

export const arrayToParams = (parentKey: string, arr: any[]): object => {
  return arr.reduce((params, element: any, index: number) => {
    if (Array.isArray(element)) {
      return { ...params, ...arrayToParams(`${parentKey}[${index}]`, element) };
    } else if (typeof element === 'object') {
      return {
        ...params,
        ...subObjectToParams(`${parentKey}[${index}]`, element)
      };
    }

    params[`${parentKey}[${index}]`] =
      typeof element === 'boolean' ? +element : element;
    return params;
  }, {});
};
