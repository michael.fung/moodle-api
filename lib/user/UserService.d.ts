import HttpClient from '../utils/HttpClient';
import { ICriteria } from '../common/Common';
import { IUser } from './User';
export default class UserService {
    private httpClient;
    private static instance;
    static getInstance: (httpClient: HttpClient) => UserService;
    constructor(httpClient: HttpClient);
    /**
     * Search Users
     *
     * @param {ICriteria} criteria
     */
    search: (criteria: ICriteria[]) => Promise<any>;
    /**
     * Update Users
     *
     * @param {IUser[]} users
     */
    update: (users: IUser[]) => Promise<any>;
}
