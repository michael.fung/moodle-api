import HttpClient from '../utils/HttpClient';
import { ICriteria } from '../common/Common';
import { ICategory } from './Category';
export default class CategoryService {
    private httpClient;
    private static instance;
    static getInstance: (httpClient: HttpClient) => CategoryService;
    constructor(httpClient: HttpClient);
    /**
     * @param {Criteria[]} criteria
     */
    search: (criteria: ICriteria[]) => Promise<any>;
    /**
     * @param {Category[]} category
     */
    create: (categories: ICategory[]) => Promise<any>;
}
