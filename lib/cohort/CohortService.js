"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ParamBuilder_1 = require("../utils/ParamBuilder");
var ContextScope;
(function (ContextScope) {
    ContextScope["self"] = "self";
    ContextScope["parents"] = "parents";
    ContextScope["all"] = "all";
})(ContextScope || (ContextScope = {}));
var CohortService = /** @class */ (function () {
    function CohortService(httpClient) {
        var _this = this;
        this.httpClient = httpClient;
        /**
         * Search Cohorts
         *
         * @param {ICohortContext | IContext} context
         * @param {ContextScope} includes {'What other contexts to fetch the frameworks from. (all, parents, self)'}
         * @param {number} limitFrom {'limitfrom we are fetching the records from'}
         * @param {number} limitNum {'Number of records to fetch'}
         * @param {string} query {'Query string'}
         */
        this.search = function (context, includes, limitFrom, limitNum, query) {
            if (includes === void 0) { includes = ContextScope.parents; }
            if (limitFrom === void 0) { limitFrom = 0; }
            if (limitNum === void 0) { limitNum = 1; }
            if (query === void 0) { query = ''; }
            return __awaiter(_this, void 0, void 0, function () {
                var params;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            params = ParamBuilder_1.objectToParams({
                                wsfunction: 'core_cohort_search_cohorts',
                                includes: includes,
                                limitfrom: limitFrom,
                                limitnum: limitNum,
                                query: query,
                                context: context
                            });
                            return [4 /*yield*/, this.httpClient.get(params)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        /**
         * Create Cohorts
         *
         * @param {Cohort[]} cohort
         */
        this.create = function (cohorts) { return __awaiter(_this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = ParamBuilder_1.objectToParams({
                            wsfunction: 'core_cohort_create_cohorts',
                            cohorts: cohorts
                        });
                        return [4 /*yield*/, this.httpClient.get(params)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        }); };
        this.addMembers = function (members) { return __awaiter(_this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = ParamBuilder_1.objectToParams({
                            wsfunction: 'core_cohort_add_cohort_members',
                            members: members
                        });
                        return [4 /*yield*/, this.httpClient.get(params)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        }); };
        this.getMembers = function (cohortids) { return __awaiter(_this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = ParamBuilder_1.objectToParams({
                            wsfunction: 'core_cohort_get_cohort_members',
                            cohortids: cohortids
                        });
                        return [4 /*yield*/, this.httpClient.get(params)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        }); };
    }
    CohortService.getInstance = function (httpClient) {
        if (!CohortService.instance) {
            CohortService.instance = new CohortService(httpClient);
        }
        return CohortService.instance;
    };
    return CohortService;
}());
exports.default = CohortService;
