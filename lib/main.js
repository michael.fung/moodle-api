"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HttpClient_1 = require("./utils/HttpClient");
var CategoryService_1 = require("./category/CategoryService");
var CohortService_1 = require("./cohort/CohortService");
var UserService_1 = require("./user/UserService");
var Main = /** @class */ (function () {
    function Main() {
        this.init = function (baseUrl, token) {
            var httpClient = HttpClient_1.default.getInstance(baseUrl, token);
            return {
                categories: CategoryService_1.default.getInstance(httpClient),
                cohorts: CohortService_1.default.getInstance(httpClient),
                users: UserService_1.default.getInstance(httpClient)
            };
        };
    }
    Main.getInstance = function () {
        if (!Main.instance) {
            Main.instance = new Main();
        }
        return Main.instance;
    };
    return Main;
}());
exports.MoodleApi = function (baseUrl, token) {
    return Main.getInstance().init(baseUrl, token);
};
