import CategoryService from "./category/CategoryService";
import CohortService from "./cohort/CohortService";
import UserService from "./user/UserService";
export declare const MoodleApi: (baseUrl: string, token: string) => {
    categories: CategoryService;
    cohorts: CohortService;
    users: UserService;
};
