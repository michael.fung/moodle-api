"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.objectToParams = function (obj) {
    return Object.keys(obj).reduce(function (params, key) {
        var element = obj[key];
        if (Array.isArray(element)) {
            return __assign({}, params, exports.arrayToParams(key, element));
        }
        else if (typeof element === 'object') {
            return __assign({}, params, exports.subObjectToParams(key, element));
        }
        params[key] = typeof element === 'boolean' ? +element : element;
        return params;
    }, {});
};
exports.subObjectToParams = function (parentKey, obj) {
    return Object.keys(obj).reduce(function (params, key) {
        var element = obj[key];
        if (typeof element === 'object') {
            return __assign({}, params, exports.subObjectToParams(parentKey + "[" + key + "]", element));
        }
        params[parentKey + "[" + key + "]"] =
            typeof element === 'boolean' ? +element : element;
        return params;
    }, {});
};
exports.arrayToParams = function (parentKey, arr) {
    return arr.reduce(function (params, element, index) {
        if (Array.isArray(element)) {
            return __assign({}, params, exports.arrayToParams(parentKey + "[" + index + "]", element));
        }
        else if (typeof element === 'object') {
            return __assign({}, params, exports.subObjectToParams(parentKey + "[" + index + "]", element));
        }
        params[parentKey + "[" + index + "]"] =
            typeof element === 'boolean' ? +element : element;
        return params;
    }, {});
};
