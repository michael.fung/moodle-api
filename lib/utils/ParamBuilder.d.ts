export declare const objectToParams: (obj: object) => object;
export declare const subObjectToParams: (parentKey: string, obj: object) => object;
export declare const arrayToParams: (parentKey: string, arr: any[]) => object;
