export default class HttpClient {
    private baseUrl;
    private static instance;
    static getInstance: (baseUrl: string, token: string) => HttpClient;
    private config;
    constructor(baseUrl: string, token: string);
    get: (params: object) => Promise<any>;
    post: (params: object) => Promise<any>;
    private returnData;
}
